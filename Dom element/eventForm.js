//protect the problem about code
"use strict";

function listForElement() {
    var formElements = document.getElementsByTagName('form');
    console.log(formElements)
}

function copyTextFormNameToLastName() {
    var nameElement = document.getElementById('firstname');
    var surnameElement = document.getElementById('lastname');
    var firstName = nameElement.value;
    console.log('firstname is ' + firstName);
    console.log('lastname is ' + surnameElement.value)

    surnameElement.value = firstName;
    console.log('now lastname is ' + surnameElement.value)
}

function resetFormData() {
    document.getElementById('firstname').value = "";
    document.getElementById('lastname').value = "";
    document.getElementById('email').value = "";
    document.getElementById('password').value = "";
    document.getElementById('confirmPassword').value = "";
    document.getElementById('radio1').checked = false;
    document.getElementById('radio2').checked = false;
    document.getElementById('invalidCheck').checked = false;
    console.log('reset!');
}

function showRadioValueInEmailBox() {
    var radioElements = document.getElementsByName('stuStatus');

    for (var i = 0; i < radioElements.length; i++) {
        if (radioElements[i].checked) {
            document.getElementById('email').value = radioElements[i].value;
            console.log('radio: ' + radioElements[i].value);

        }
    }
}

function showSummary() {
    var name = document.getElementById('firstname').value;
    var lastName = document.getElementById('lastname').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('password').value;
    var confirm_password = document.getElementById('confirmPassword').value;
    var radio1 = document.getElementById('radio1');
    var radio2 = document.getElementById('radio2');

    var summaryElement = document.getElementById('summary');
    if (name == "" && lastName == "" && email == "" && password == "" && confirm_password == "" && radio1.checked == false
        && radio2.checked == false) {
        summaryElement.setAttribute("hidden", true);
        console.log('no data in form!')
    }
    else {
        if (name != "") {
            summaryElement.innerHTML = "Name: " + name + "<br>";
        }
        if (lastName != "") {
            summaryElement.innerHTML = summaryElement.innerHTML + "Lastname: " + lastName + "<br>";
        }
        if (email != "") {
            summaryElement.innerHTML = summaryElement.innerHTML + "Email: " + email + "<br>";
        }
        if (password != "") {
            summaryElement.innerHTML = summaryElement.innerHTML + "Password: " + password + "<br>";
        }
        if (confirm_password != "") {
            summaryElement.innerHTML = summaryElement.innerHTML + "Confirm password: " + confirm_password + "<br>";
        }
        if (radio1.checked != false) {
            summaryElement.innerHTML = summaryElement.innerHTML + "Student status: " + radio1.value + "<br>";
        }
        if (radio2.checked != false) {
            summaryElement.innerHTML = summaryElement.innerHTML + "Student status: " + radio2.value + "<br>";
        }
        summaryElement.removeAttribute('hidden');
    }

}

function addPElement(){
    var node = document.createElement('p');
    node.innerHTML = 'Hello world';
    var placeHolder = document.getElementById('placeholder');
    placeHolder.appendChild(node);
}

function addHref(){
    var node = document.createElement('a');
    node.innerHTML = 'google';
    node.setAttribute('href', 'https://google.com');
    node.setAttribute('target', '_blank');
    var placeHolder = document.getElementById('placeholder');
    placeHolder.appendChild(node);
}

function addBoth(){
    var node = document.createElement('p');
    node.innerHTML = 'Hello world';
    var node2 = document.createElement('a');
    node2.innerHTML = 'google';
    node2.setAttribute('href', 'https://google.com');
    var placeHolder = document.getElementById('placeholder');
    placeHolder.appendChild(node);
    placeHolder.appendChild(node2);
}